<?php
require_once __DIR__.'/../vendor/autoload.php';

use App\Router;
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__.'../../');
$dotenv->load();

define("URL",getenv('APP_URL'));
define("ASSET",getenv('APP_URL').'/assets');
$app = new Router();

$app->get('/', function () {
    return \App\Controller\DashboardController::index();
});

$app->get('/products', function () {
    return \App\Controller\ProductController::index();
});
$app->get('/products/create', function () {
    return \App\Controller\ProductController::create();
});
$app->post('/products/save', function () {
    return \App\Controller\ProductController::save();
});
$app->get('/products/edit', function () {
    return \App\Controller\ProductController::edit();
});
$app->post('/products/update', function () {
    return \App\Controller\ProductController::update();
});
$app->get('/products/delete', function () {
    return \App\Controller\ProductController::delete();
});


$app->get('/categories', function () {
    return \App\Controller\CategoriesController::index();
});

$app->get('/categories/create', function () {
    return \App\Controller\CategoriesController::create();
});

$app->post('/categories/save', function () {
    return \App\Controller\CategoriesController::save();
});

$app->get('/categories/edit', function () {
    return \App\Controller\CategoriesController::edit();
});

$app->post('/categories/update', function () {
    return \App\Controller\CategoriesController::update();
});

$app->get('/categories/delete', function () {
    return \App\Controller\CategoriesController::delete();
});

$app->run();