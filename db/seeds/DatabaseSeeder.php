<?php
use Phinx\Seed\AbstractSeed;
use App\Model\Product;
use App\Model\Category;
use App\Model\ProductCategory;
class DatabaseSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $handle = fopen(__DIR__."/import.csv", "r");
        $header = true;
        try{
            while ($csvLine = fgetcsv($handle, 1000, ";")) {
                if ($header) {
                    $header = false;
                } else {
                    $prod = new Product;
                    $produto = $prod->create([
                        $csvLine[0],
                        $csvLine[1],
                        $csvLine[4],
                        $csvLine[2],
                        $csvLine[3],
                    ]);
                    $categorias = explode('|',$csvLine[5]);
                    foreach($categorias as $key => $cat)
                    {
                        $cate = new Category;
                        $categoria = $cate->findOrCreate('name',$cat,[$cat,$key.'CAT#'.Date('his').rand(1,99)]);
                        $catprod = new ProductCategory;
                        $catprod->create([$produto['id'],$categoria['id']]);
                    }
                }
            }
        }
        catch(\Exception $e)
        {
            die($e);
        }
    }
}
