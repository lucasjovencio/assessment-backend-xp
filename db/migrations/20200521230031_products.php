<?php

use Phinx\Migration\AbstractMigration;

class Products extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('products')
            ->addColumn('name', 'string')
            ->addColumn('sku', 'string')
            ->addColumn('price', 'decimal',['precision'=>8,'scale'=>4])
            ->addColumn('description','text')
            ->addColumn('quantity','integer')
            ->addColumn('photo', 'string',['null' => true])
            ->addColumn('updated_at', 'datetime',['default'=>'CURRENT_TIMESTAMP'])
            ->addColumn('created_at', 'datetime',['default'=>'CURRENT_TIMESTAMP'])
            ->addColumn('deleted_at', 'datetime',['null' => true])
            // ->addIndex(['sku'], ['unique' => true])
            ->create();
    }
}
