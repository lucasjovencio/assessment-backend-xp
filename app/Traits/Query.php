<?php

namespace App\Traits;
use PDO;

trait Query 
{
    function mysql_escape_mimic($inp) {
        if(is_array($inp))
            return array_map(__METHOD__, $inp);
        if(!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a",','), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z','\,'), $inp);
        }
        return $inp;
    }

    public function prepare_value()
    {
        $new_dados=[];
        foreach($this->fillable as $fill)
        {
            $new_dados[]=':'.$fill;
        }
        return $new_dados;
    }
    
    public function prepare_insert($dados)
    {
        $new_dados=[];
        foreach($dados as $key => $dado)
        {
            $new_dados[':'.$this->fillable[$key]]=$dado;//$this->mysql_escape_mimic($dado);
        }
        return $new_dados;
    }

    public function get()
	{
		return $this->data;
    }
    
    public function create($dados)
    {
        $db = $this->db();
        $db->prepare("
            INSERT INTO {$this->table} (".implode(',',$this->fillable).") 
            VALUES (".implode(',',$this->prepare_value()).") 
        ")->execute($this->prepare_insert($dados));
        $this->data = $this->find($db->lastInsertId());
        return $this->data;
    }

    public function prepare_update($dados)
    {
        $new_dados=[];
        foreach($dados as $key => $dado)
        {
            $new_dados[]=$this->fillable[$key].'=:'.$this->fillable[$key];
        }
        return $new_dados;
    }

    public function update($id,$dados)
    {
        $db = $this->db();
        foreach($dados as $key => $dt){
            $prepare = $db->prepare(" UPDATE {$this->table} SET ".$this->fillable[$key]."=:".$this->fillable[$key]." where id={$id}");
            $prepare->bindParam(":".$this->fillable[$key],$dt);
            $prepare->execute();
        }
        return $this;
    }

    public function whereId($id)
    {
        $this->data = $this->db()->query("
            SELECT * FROM {$this->table} WHERE id='{$id}' AND deleted_at IS NULL
        ",PDO::FETCH_ASSOC)->fetch();
        return $this;
    }
    
    public function find($id)
    {
        $result = $this->db()->query("
            SELECT * FROM {$this->table} WHERE id='{$id}' AND deleted_at IS NULL
        ",PDO::FETCH_ASSOC)->fetch();
        $this->data = $result;
        if(!$result)
            return null;
        return $result;
    }

    public function findOrCreate($column,$value,$dados)
    {
        $result = $this->db()->query("
            SELECT * FROM {$this->table} WHERE {$column}='{$value}'
        ",PDO::FETCH_ASSOC)->fetch();
        if(!$result){
            return $this->create($dados);
        }
        $this->data = $result;
        return $result;
    }

    public function all()
    {
        $this->data = $this->db()->query("SELECT * FROM {$this->table} where deleted_at IS NULL",PDO::FETCH_ASSOC)->fetchAll();
        return $this;
    }

    public function count()
    {
        $this->count = ($this->db()->query("SELECT SUM(1) AS count FROM {$this->table} where deleted_at IS NULL",PDO::FETCH_ASSOC)->fetch())['count'];
        return $this->count;
    }

    public function select($skip=0,$limit=4)
    {
        $this->data = $this->db()->query("SELECT * FROM {$this->table}  where deleted_at IS NULL ORDER BY created_at DESC LIMIT {$limit} OFFSET {$skip} ",PDO::FETCH_ASSOC)->fetchAll();
        return $this;
    }

    public function last4($limit=4)
    {
        $this->data = $this->db()->query("SELECT * FROM {$this->table} where deleted_at IS NULL ORDER BY created_at DESC LIMIT {$limit} ",PDO::FETCH_ASSOC)->fetchAll();
        return $this;
    }

    public function delete($id)
    {
        $delete_at = Date('Y-m-d h:i:s');
        $this->data = $this->db()->prepare("UPDATE {$this->table} SET `deleted_at` = '{$delete_at}' WHERE (`id` = '${id}')")->execute();
        return null;
    }
}