<?php
namespace App\Controller;

use App\Model\Product;
final class DashboardController extends Controller {

    public function __construct(){
        $_SESSION['title']='Dashboard';
    }

    public static function index() {
        self::title('Dashboard');
        $produto = new Product;
        return self::view('dashboard',['count'=> $produto->count(),'products'=>$produto->last4()->get()]);
    }
}