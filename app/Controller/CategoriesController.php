<?php
namespace App\Controller;

use App\Model\Category;
use App\Router;
final class CategoriesController extends Controller {

    public static function index() {
        self::title('Categories');
        $category = new Category;
        $count = $category->count();
        if(self::params('categories-page')){
            self::session('categories-page',self::params('categories-page'));
            self::session('categories-skip',self::params('categories-skip'));
            self::session('categories-skip',self::params('categories-skip'));
            self::session('categories-skip-show',(self::params('categories-skip') > $count) ? false : true);
            $skip = (self::params('categories-skip') > $count) ? $count-4 : self::params('categories-skip');
        }else{
            self::session('categories-page',1);
            self::session('categories-skip',0);
            self::session('categories-skip-show',true);
            $skip = 0;
        }
        return self::view('categories',['categories'=>$category->select($skip,4)->get()]);
    }

    public static function edit() {
        self::title('Categories');
        $category = new Category;
        return self::view('editCategory',['category'=>$category->find(self::params('id'))]);
    }

    public static function create() {
        self::title('Add Category');
        return self::view('addCategory');
    }

    public static function save() {
        $fillable = ['name','code'];
        $erros = self::validate($fillable);
        if(count($erros)){
            return self::redirect('/categories/create','required',$erros);
        }
        try{
            $prod = new Category;
            $category = $prod->create([
                self::params('name'),
                self::params('code'),
            ]);
            return self::redirect('/categories','success','Category successfully created!');
        }catch(\Exception $e){
            return self::redirect('/categories','danger','An error has occurred, please try again.');
        }
    }

    public static function update()
    {
        $fillable = ['name','code'];
        $erros = self::validate($fillable);
        $id=self::params('id');
        if(count($erros)){
            return self::redirect("/categories/edit?id={$id}",'required',$erros);
        }
        try{
            $cate = new Category;
            $cate->update($id,[
                self::params('name'),
                self::params('code'),
            ]);
            return self::redirect("/categories/edit?id={$id}",'success','Category successfully updated!');
        }catch(\Exception $e){
            return self::redirect('/categories','danger','An error has occurred, please try again.');
        }
    }

    public static function delete() {
        try{
            (new Category)->delete(self::params('id'));
            return self::redirect('/categories','success','Category successfully deleted!');
        }catch(\Exception $e){
            return self::redirect('/categories','danger','An error has occurred, please try again.');
        }
    }
}