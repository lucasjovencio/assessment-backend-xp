<?php
namespace App\Controller;

use App\Model\Product;
use App\Model\Category;
use App\Model\ProductCategory;
use App\Router;
final class ProductController extends Controller {

    public static function index() {
        self::title('Products');
        $produto = new Product;
        $count = $produto->count();
        if(self::params('products-page')){
            self::session('products-page',self::params('products-page'));
            self::session('products-skip',self::params('products-skip'));
            self::session('products-skip',self::params('products-skip'));
            self::session('products-skip-show',(self::params('products-skip') > $count) ? false : true);
            $skip = (self::params('products-skip') > $count) ? $count-4 : self::params('products-skip');
        }else{
            self::session('products-page',1);
            self::session('products-skip',0);
            self::session('products-skip-show',true);
            $skip = 0;
        }
        return self::view('products',['products'=>$produto->select($skip,4)->categorias()->get()]);
    }

    public static function edit() {
        self::title('Products');
        $produto = new Product;
        $categoria = new Category;
        return self::view('editProduct',[
                'product'=>$produto->whereId(self::params('id'))->categoriasOne()->get(),
                'categories'=>$categoria->all()->get()
            ]);
    }

    public static function create() {
        self::title('Add Product');
        $categoria = new Category;
        return self::view('addProduct',['categories'=>$categoria->all()->get()]);
    }

    public static function save() {
        $fillable = ['name','sku','price','description','quantity','category'];
        $erros = self::validate($fillable);
        if(count($erros)){
            return self::redirect('/products/create','required',$erros);
        }
        try{
            $prod = new Product;
            $produto = $prod->create([
                self::params('name'),
                self::params('sku'),
                doubleval(self::params('price')),
                self::params('description'),
                (int)self::params('quantity')
            ]);
            foreach(self::params('category') as $cat)
            {
                $cate = new Category;
                $categoria = $cate->find($cat);
                $catprod = new ProductCategory;
                $catprod->create([$produto['id'],$categoria['id']]);
            }
            return self::redirect('/products','success','Product successfully created!');
        }catch(\Exception $e){
            return self::redirect('/products','danger','An error has occurred, please try again.');
        }
        
    }

    public static function update()
    {
        $fillable = ['name','sku','price','description','quantity','category','id'];
        $erros = self::validate($fillable);
        $id=self::params('id');
        if(count($erros)){
            return self::redirect("/products/edit?id={$id}",'required',$erros);
        }

        try{
            $prod       = new Product;
            $cate       = new Category;
            $catprod    = new ProductCategory;
            
            $prod->update($id,[
                self::params('name'),
                self::params('sku'),
                doubleval(str_replace(',','',self::params('price'))),
                self::params('description'),
                (int)self::params('quantity')
            ]);
            $produto = $prod->whereId(self::params('id'))->categoriasOne()->get();
            foreach($produto['categorias'] as $cat){
                $prodCat    = new ProductCategory;
                $prodCat->delete($cat['produto_categorias_id']);
            }
            foreach(self::params('category') as $cat)
            {
                $categoria = $cate->find($cat);
                $catprod->create([$produto['id'],$categoria['id']]);
            }
            return self::redirect("/products/edit?id={$id}",'success','Product successfully updated!');
        }catch(\Exception $e){
            return self::redirect('/products','danger','An error has occurred, please try again.');
        }
    }

    public static function delete() {
        try{
            $produto = new Product;
            $produto->delete(self::params('id'));
            return self::redirect('/products','success','Product successfully deleted!');
        }catch(\Exception $e){
            return self::redirect('/products','danger','An error has occurred, please try again.');
        }
    }
}