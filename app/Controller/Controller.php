<?php
namespace App\Controller;
use App\Router;
session_start();
abstract class Controller {

    protected final function view(string $_name, array $vars = []) {
        $_filename = __DIR__."/../../views/{$_name}.php";
        if(!file_exists($_filename))
            die("View {$_name} not found!");
        
        include_once $_filename;
    }

    protected final function params(string $name) {
        $params = Router::getRequest();
        if(!isset($params[$name]))
            return null;
        return $params[$name];
    }

    protected final function redirect(string $to,$session = null,$value=null) {
        if($session){
            $_SESSION[$session]=$value; 
        }
        header('Location:' . URL . $to);
        exit();
    }

    protected final function validate($fillable)
    {
        $erros = [];
        foreach($fillable as $fill){
            if(!self::params($fill)) {
                $erros[] = "The [ {$fill} ] field is required";
            }
        }
        return $erros;
    }
    
    public static function title($title)
    {
        $_SESSION['title']=$title;
    }

    public static function session($key,$value)
    {
        $_SESSION[$key]=$value;
    }
}