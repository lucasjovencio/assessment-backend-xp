<?php
namespace App\Model;
use App\Traits\Query;
use PDO;
class Product extends Database{
	use Query;
	protected $table = "products";
	protected $fillable = ['name','sku','price','description','quantity'];
	protected $data;
	protected $count;

	public function __construct()
	{
		parent::__construct();
	}

	public function categorias()
	{
		foreach($this->data as $key => $dt){
			$dt['categories'] = $this->db()->query("SELECT 
				categories.id as id,categories.name as name,categories.code as code,product_categories.id as product_categories_id
				FROM
					categories
						INNER JOIN product_categories 
						ON product_categories.categories_id = categories.id
						where product_categories.products_id={$dt['id']} AND product_categories.deleted_at IS NULL
			",PDO::FETCH_ASSOC)->fetchAll();
			$this->data[$key] = $dt;
		}
		return $this;
	}

	public function categoriasOne()
	{
		$this->data['categories'] = $this->db()->query("SELECT 
			categories.id as id,categories.name as name,categories.code as code,product_categories.id as product_categories_id
			FROM
				categories
					INNER JOIN product_categories 
					ON product_categories.categories_id = categories.id
					where product_categories.products_id={$this->data['id']} AND product_categories.deleted_at IS NULL
		",PDO::FETCH_ASSOC)->fetchAll();
		return $this;
	}
}