<?php
namespace App\Model;
use App\Traits\Query;
class ProductCategory extends Database{
    use Query;
    protected $table = "product_categories";
    protected $fillable = ['products_id','categories_id'];
    protected $data;
    protected $count;
    public function __construct()
    {
        parent::__construct();
    }
}