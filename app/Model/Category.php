<?php
namespace App\Model;
use App\Traits\Query;
class Category extends Database{
	use Query;
	protected $table = "categories";
	protected $fillable = ['name','code'];
	protected $data;
	protected $count;
	public function __construct()
	{
		parent::__construct();
	}
}