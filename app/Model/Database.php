<?php

namespace App\Model;
use PDO;
use Dotenv\Dotenv;

abstract class Database {
    private $connection;

    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(__DIR__.'../../../');
        $dotenv->load();
    }

    public function con(){
        try {
            
            $this->connection = new PDO(getenv('DB_CONNECTION').":host=".getenv('DB_HOST').":".getenv('DB_PORT').";dbname=".getenv('DB_DATABASE')."", getenv('DB_USERNAME'), getenv('DB_PASSWORD'),
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(\PDOException $e) {
            die('ERROR: Database connection');
        }
    }
    public function db()
    {
        $this->con();
        return $this->connection;
    }
}   