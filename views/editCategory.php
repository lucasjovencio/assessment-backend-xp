
  <?php require __DIR__."/layout/header.php" ?>
  <main class="content">
    <h1 class="title new-item">Edit Category</h1>
    <?php require __DIR__."/layout/alerts.php" ?>
    <form  method="post" action="<?= URL ?>/categories/update" >
      <input type="hidden" name="id" value="<?= $vars['category']['id'] ?>">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="name" value="<?=$vars['category']['name']?>" required class="input-text" />
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="code" value="<?=$vars['category']['code']?>" required class="input-text" />
      </div>
      <div class="actions-form">
        <a href="<?=URL?>/categories" class="action back">Back</a>
        <button class="btn-submit btn-action" type="submit">Update Category</button>
      </div>
    </form>
  </main>
  <!-- Main Content -->
<?php require __DIR__."/layout/footer.php" ?>