
  <?php require __DIR__."/layout/header.php" ?>
  <main class="content">
    <h1 class="title new-item">New Product</h1>
    <?php require __DIR__."/layout/alerts.php" ?>
    <form  method="post" action="<?= URL ?>/products/save" >
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" required/> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="category[]" class="input-text"  required>
          <?php foreach($vars['categories'] as $categoria){ ?>
            <option value="<?= $categoria['id'] ?>"><?= $categoria['name']?> <?= $categoria['code'] ?> </option>
          <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"  required></textarea>
      </div>
      <div class="actions-form">
        <a href="<?=URL?>/produtos" class="action back">Back</a>
        <button class="btn-submit btn-action" type="submit">Save Product</button>
      </div>
      
    </form>
  </main>
  <!-- Main Content -->
<?php require __DIR__."/layout/footer.php" ?>
<script src="<?=ASSET?>/js/jquery-2.2.4.min.js"></script>
<script src="<?=ASSET?>/js/mask/src/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js" integrity="sha256-U0YLVHo5+B3q9VEC4BJqRngDIRFCjrhAIZooLdqVOcs=" crossorigin="anonymous"></script>
<script>
  $('#quantity').mask("00000000", {reverse: true});
  $(function() {
		$('#price').maskMoney(
			{
				thousands:',', 
				decimal:'.', 
				prefix: '',
				affixesStay:true,
				allowNegative:false,
				allowZero:true,
			}
		);
		$("#price").focus();
		$('#price').blur();
	})
  
</script>