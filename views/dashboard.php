<?php require __DIR__."/layout/header.php" ?>
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
      You have <?= $vars['count'] ?> products added on this store: <a href="<?=URL?>/products/create" class="btn-action">Add new Product</a>
    </div>
    <ul class="product-list">
        <?php foreach($vars['products'] as $product){ ?>
            <li>
                <div class="product-image">
                    <img src="<?php if($product['photo']){ echo($product['photo']); }else{?> <?=ASSET?>/images/bt-close.png <?php } ?>" layout="responsive" width="164" height="145" alt="<?= $product['name'] ?>" />
                </div>
                <div class="product-info">
                <div class="product-name"><span><?= $product['name'] ?></span></div>
                <div class="product-price"><span class="special-price">9 available</span> <span>R$ <?= number_format($product['price'],2) ?></span></div>
                </div>
            </li>
        <?php } ?>
    </ul>
  </main>
  <!-- Main Content -->

  <?php require __DIR__."/layout/footer.php" ?>