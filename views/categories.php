<?php require __DIR__."/layout/header.php" ?>
  
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="<?=URL?>/categories/create" class="btn-action">Add new Category</a>
    </div>
    <?php require __DIR__."/layout/alerts.php" ?>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($vars['categories'] as $categoria){ ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $categoria['name'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $categoria['code'] ?></span>
          </td>
        
          <td class="data-grid-td">
            <div class="actions">
              <div class="action edit" onClick="funcEdit(<?=$categoria['id']?>)" ><span>Edit</span></div>
              <div class="action delete" onCLick="funcDelete(<?=$categoria['id']?>)"><span>Delete</span></div>
            </div>
          </td>
        </tr>
      <?php } ?>
      
    </table>
    <?php if(isset($_SESSION['categories-page']) && $_SESSION['categories-page']>1){ ?>
      <button onClick="funcPrev(<?=$_SESSION['categories-page']?>,<?=$_SESSION['categories-skip']?>)" class="button-pagination">Previous</button>
    <?php } ?>
    <p style="float: right;margin: 0px;">Page: <?=$_SESSION['categories-page']?></p>
    <?php if(isset($_SESSION['categories-page']) && count($vars['categories']) >=4 && isset($_SESSION['categories-skip-show']) && $_SESSION['categories-skip-show'] ){ ?>
      <button  onClick="funcNext(<?=$_SESSION['categories-page']?>,<?=$_SESSION['categories-skip']?>)" class="button-pagination">Next</button>
    <?php } ?>
  </main>
  <!-- Main Content -->

<script>
	function funcDelete(id){
		const delet = confirm('Do you want to remove this category?')
		if(delet){
			document.location.href = "<?=URL?>/categories/delete?id=" + id
		}
	}
	function funcEdit(id)
	{
		document.location.href = "<?=URL?>/categories/edit?id=" + id
  }
  
  function funcPrev(page,skip){
    document.location.href = "<?=URL?>/categories?categories-page=" + (page-1)+"&categories-skip="+(skip-4)
  }
  function funcNext(page,skip){
    document.location.href = "<?=URL?>/categories?categories-page=" + (page+1)+"&categories-skip="+(skip+4)
  }
</script>
<?php require __DIR__."/layout/footer.php" ?>