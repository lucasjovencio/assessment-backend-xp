<?php require __DIR__."/layout/header.php" ?>
  
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="<?=URL?>/products/create" class="btn-action">Add new Product</a>
    </div>
    <?php require __DIR__."/layout/alerts.php" ?>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($vars['products'] as $product){ ?>
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $product['name'] ?></span>
          </td>
        
          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $product['sku'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= number_format($product['price'],2) ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content"><?= $product['quantity'] ?></span>
          </td>

          <td class="data-grid-td">
            <span class="data-grid-cell-content">
              <?php foreach($product['categories'] as $categoria){ ?>  
                  <?= $categoria['name'] ?><br/>
              <?php } ?>
          </td>
        
          <td class="data-grid-td">
            <div class="actions">
              <div class="action edit" onClick="funcEdit(<?=$product['id']?>)" ><span>Edit</span></div>
              <div class="action delete" onCLick="funcDelete(<?=$product['id']?>)"><span>Delete</span></div>
            </div>
          </td>
        </tr>
      <?php } ?>
      
    </table>
    <?php if(isset($_SESSION['products-page']) && $_SESSION['products-page']>1){ ?>
      <button onClick="funcPrev(<?=$_SESSION['products-page']?>,<?=$_SESSION['products-skip']?>)" class="button-pagination">Previous</button>
    <?php } ?>
    <p style="float: right;margin: 0px;">Page: <?=$_SESSION['products-page']?></p>
    <?php if(isset($_SESSION['products-page']) && count($vars['products']) >=4 && isset($_SESSION['products-skip-show']) && $_SESSION['products-skip-show'] ){ ?>
      <button  onClick="funcNext(<?=$_SESSION['products-page']?>,<?=$_SESSION['products-skip']?>)" class="button-pagination">Next</button>
    <?php } ?>
  </main>
  <!-- Main Content -->

<script>
	function funcDelete(id){
		const delet = confirm('Do you want to remove this product?')
		if(delet){
			document.location.href = "<?=URL?>/products/delete?id=" + id
		}
	}
	function funcEdit(id)
	{
		document.location.href = "<?=URL?>/products/edit?id=" + id
  }
  
  function funcPrev(page,skip){
    document.location.href = "<?=URL?>/products?products-page=" + (page-1)+"&products-skip="+(skip-4)
  }
  function funcNext(page,skip){
    document.location.href = "<?=URL?>/products?products-page=" + (page+1)+"&products-skip="+(skip+4)
  }
</script>
<?php require __DIR__."/layout/footer.php" ?>