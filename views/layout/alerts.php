<style>
    .alert{
        width: 100%;
        min-height: 40px;
        padding: 10px;
    }
    .success{
        background: #c5e9c5;
    }
    .danger{
        background: #e9c5c5;
    }
</style>
<?php 
if(isset($_SESSION['success'])){ ?>
    <div class="alert success">
        <span><?=$_SESSION['success']?></span>
    </div>
<?php }?>

<?php if(isset($_SESSION['danger'])){?>
    <div class="alert danger">
        <span><?=$_SESSION['danger']?></span>
    </div>
<?php } ?>

<?php if(isset($_SESSION['required'])){?>
    <div class="alert danger">
        <ul>
            <?php foreach($_SESSION['required'] as $field){ ?>
                <li><?=$field?></li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>


<?php 
    if(isset($_SESSION['success'])) unset( $_SESSION['success'] );
    if(isset($_SESSION['danger'])) unset( $_SESSION['danger'] );
    if(isset($_SESSION['required'])) unset( $_SESSION['required'] );
?>